#region usings

using System;
using NUnit.Framework;
using Prismactic.Core.MVVM;

#endregion

namespace CoreMVVM.Tests
{
    [TestFixture]
    public class PropertySupportFixture
    {
        public static int StaticProperty { get; set; }

        public int InstanceProperty { get; set; }

        public int InstanceField;

        public static int SetOnlyStaticProperty
        {
            set { }
        }

        [Test]
        [ExpectedException(typeof (ArgumentNullException))]
        public void WhenExpressionIsNull_ThenAnExceptionIsThrown()
        {
            PropertySupport.ExtractPropertyName<int>(null);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void WhenExpressionRepresentsANonMemberAccessExpression_ThenAnExceptionIsThrown()
        {
            PropertySupport.ExtractPropertyName(() => GetHashCode());
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void WhenExpressionRepresentsANonPropertyMemberAccessExpression_ThenAnExceptionIsThrown()
        {
            PropertySupport.ExtractPropertyName(() => InstanceField);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void WhenExpressionRepresentsAStaticProperty_ThenExceptionThrown()
        {
            PropertySupport.ExtractPropertyName(() => StaticProperty);
        }

        [Test]
        public virtual void WhenExtractingNameFromAValidPropertyExpression_ThenPropertyNameReturned()
        {
            var propertyName = PropertySupport.ExtractPropertyName(() => InstanceProperty);
            Assert.AreEqual("InstanceProperty", propertyName);
        }
    }
}