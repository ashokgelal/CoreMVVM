using Prismactic.Core.MVVM;

namespace CoreMVVM.Tests
{
    public class MockViewModel : BindableBase
    {
        private int _mockProperty;

        public int MockProperty
        {
            get { return _mockProperty; }

            set { SetProperty(ref _mockProperty, value); }
        }

        internal void InvokeOnPropertyChanged()
        {
            OnPropertyChanged(() => MockProperty);
        }
    }
}