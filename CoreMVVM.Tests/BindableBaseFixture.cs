#region usings

using NUnit.Framework;

#endregion

namespace CoreMVVM.Tests
{
    [TestFixture]
    public class BindableBaseFixture
    {
        [Test]
        public void OnPropertyChangedShouldExtractPropertyNameCorrectly()
        {
            var invoked = false;
            var mockViewModel = new MockViewModel();

            mockViewModel.PropertyChanged += (o, e) => { if (e.PropertyName.Equals("MockProperty")) invoked = true; };
            mockViewModel.InvokeOnPropertyChanged();

            Assert.IsTrue(invoked);
        }

        [Test]
        public void SetPropertyMethodShouldRaisePropertyRaised()
        {
            var invoked = false;
            var mockViewModel = new MockViewModel();

            mockViewModel.PropertyChanged += (o, e) => { if (e.PropertyName.Equals("MockProperty")) invoked = true; };
            mockViewModel.MockProperty = 10;

            Assert.IsTrue(invoked);
        }

        [Test]
        public void SetPropertyMethodShouldSetTheNewValue()
        {
            var value = 10;
            var mockViewModel = new MockViewModel();

            Assert.AreEqual(mockViewModel.MockProperty, 0);

            mockViewModel.MockProperty = value;
            Assert.AreEqual(mockViewModel.MockProperty, value);
        }
    }
}