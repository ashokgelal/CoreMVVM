﻿#region usings

using System;
using System.Windows.Input;
using NUnit.Framework;
using Prismactic.Core.MVVM;
using Prismactic.Core.MVVM.Commands;

#endregion

namespace CoreMVVM.Tests.Commands
{
    [TestFixture]
    public class CompositeCommandFixture
    {
        private class CanExecutChangeHandler
        {
            public void CanExecuteChangeHandler(object sender, EventArgs e)
            {
            }
        }

        [Test]
        public void ActivityCausesActiveAwareCommandToRequeryCanExecute()
        {
            var activeAwareCommand = new CompositeCommand(true);
            var command = new MockActiveAwareCommand();
            activeAwareCommand.RegisterCommand(command);
            command.IsActive = true;

            var globalCanExecuteChangeFired = false;
            activeAwareCommand.CanExecuteChanged += delegate { globalCanExecuteChangeFired = true; };

            Assert.IsFalse(globalCanExecuteChangeFired);
            command.IsActive = false;
            Assert.IsTrue(globalCanExecuteChangeFired);
        }

        [Test]
        public void CanExecuteShouldReturnFalseIfASingleRegistrantsFalse()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand {CanExecuteValue = true};
            var testCommandTwo = new TestCommand {CanExecuteValue = false};

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.RegisterCommand(testCommandTwo);

            Assert.IsFalse(multiCommand.CanExecute(null));
        }

        [Test]
        public void CanExecuteShouldReturnFalseWithNoCommandsRegistered()
        {
            var multiCommand = new TestableCompositeCommand();
            Assert.IsFalse(multiCommand.CanExecute(null));
        }

        [Test]
        public void CanExecuteShouldReturnTrueIfAllRegistrantsTrue()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand {CanExecuteValue = true};
            var testCommandTwo = new TestCommand {CanExecuteValue = true};

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.RegisterCommand(testCommandTwo);

            Assert.IsTrue(multiCommand.CanExecute(null));
        }

        [Test]
        public void DispatchCommandDoesNotIncludeInactiveRegisteredCommandInVoting()
        {
            var activeAwareCommand = new CompositeCommand(true);
            var command = new MockActiveAwareCommand();
            activeAwareCommand.RegisterCommand(command);
            command.IsValid = true;
            command.IsActive = false;

            Assert.IsFalse(activeAwareCommand.CanExecute(null),
                "Registered Click is inactive so should not participate in CanExecute vote");

            command.IsActive = true;

            Assert.IsTrue(activeAwareCommand.CanExecute(null));

            command.IsValid = false;

            Assert.IsFalse(activeAwareCommand.CanExecute(null));
        }

        [Test]
        public void DispatchCommandShouldIgnoreInactiveCommandsInCanExecuteVote()
        {
            var activeAwareCommand = new CompositeCommand(true);
            var commandOne = new MockActiveAwareCommand {IsActive = false, IsValid = false};
            var commandTwo = new MockActiveAwareCommand {IsActive = true, IsValid = true};

            activeAwareCommand.RegisterCommand(commandOne);
            activeAwareCommand.RegisterCommand(commandTwo);

            Assert.IsTrue(activeAwareCommand.CanExecute(null));
        }

        [Test]
        public void ExecuteDoesNotThrowWhenAnExecuteCommandModifiesTheCommandsCollection()
        {
            var multiCommand = new TestableCompositeCommand();
            var commandOne = new SelfUnregisterableCommand(multiCommand);
            var commandTwo = new SelfUnregisterableCommand(multiCommand);

            multiCommand.RegisterCommand(commandOne);
            multiCommand.RegisterCommand(commandTwo);

            multiCommand.Execute(null);

            Assert.IsTrue(commandOne.ExecutedCalled);
            Assert.IsTrue(commandTwo.ExecutedCalled);
        }

        [Test]
        public void MultiDispatchCommandDoesNotExecutesInactiveRegisteredCommands()
        {
            var activeAwareCommand = new CompositeCommand(true);
            var command = new MockActiveAwareCommand {IsActive = false};
            activeAwareCommand.RegisterCommand(command);

            activeAwareCommand.Execute(null);

            Assert.IsFalse(command.WasExecuted);
        }

        [Test]
        public void MultiDispatchCommandExecutesActiveRegisteredCommands()
        {
            var activeAwareCommand = new CompositeCommand();
            var command = new MockActiveAwareCommand {IsActive = true};
            activeAwareCommand.RegisterCommand(command);

            activeAwareCommand.Execute(null);

            Assert.IsTrue(command.WasExecuted);
        }

        [Test]
        public void RegisterACommandShouldRaiseCanExecuteEvent()
        {
            var multiCommand = new TestableCompositeCommand();
            multiCommand.RegisterCommand(new TestCommand());
            Assert.IsTrue(multiCommand.CanExecuteChangedRaised);
        }

        [Test]
        [ExpectedException(typeof (ArgumentException))]
        public void RegisteringCommandInItselfThrows()
        {
            var compositeCommand = new CompositeCommand();

            compositeCommand.RegisterCommand(compositeCommand);
        }

        [Test]
        [ExpectedException(typeof (InvalidOperationException))]
        public void RegisteringCommandTwiceThrows()
        {
            var compositeCommand = new CompositeCommand();
            var duplicateCommand = new TestCommand();
            compositeCommand.RegisterCommand(duplicateCommand);

            compositeCommand.RegisterCommand(duplicateCommand);
        }

        [Test, ExpectedException(typeof (DivideByZeroException))]
        public void ShouldBubbleException()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommand = new BadDivisionCommand();

            multiCommand.RegisterCommand(testCommand);
            multiCommand.Execute(null);
        }

        [Test]
        public void ShouldDelegateCanExecuteToMultipleRegistrants()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand();
            var testCommandTwo = new TestCommand();

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.RegisterCommand(testCommandTwo);

            Assert.IsFalse(testCommandOne.CanExecuteCalled);
            Assert.IsFalse(testCommandTwo.CanExecuteCalled);
            multiCommand.CanExecute(null);
            Assert.IsTrue(testCommandOne.CanExecuteCalled);
            Assert.IsTrue(testCommandTwo.CanExecuteCalled);
        }

        [Test]
        public void ShouldDelegateCanExecuteToSingleRegistrant()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommand = new TestCommand();

            multiCommand.RegisterCommand(testCommand);

            Assert.IsFalse(testCommand.CanExecuteCalled);
            multiCommand.CanExecute(null);
            Assert.IsTrue(testCommand.CanExecuteCalled);
        }

        [Test]
        public void ShouldDelegateExecuteToMultipleRegistrants()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand();
            var testCommandTwo = new TestCommand();

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.RegisterCommand(testCommandTwo);

            Assert.IsFalse(testCommandOne.ExecuteCalled);
            Assert.IsFalse(testCommandTwo.ExecuteCalled);
            multiCommand.Execute(null);
            Assert.IsTrue(testCommandOne.ExecuteCalled);
            Assert.IsTrue(testCommandTwo.ExecuteCalled);
        }

        [Test]
        public void ShouldDelegateExecuteToSingleRegistrant()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommand = new TestCommand();

            multiCommand.RegisterCommand(testCommand);

            Assert.IsFalse(testCommand.ExecuteCalled);
            multiCommand.Execute(null);
            Assert.IsTrue(testCommand.ExecuteCalled);
        }

        [Test]
        public void ShouldIgnoreChangesToIsActiveDuringExecution()
        {
            var firstCommand = new MockActiveAwareCommand {IsActive = true};
            var secondCommand = new MockActiveAwareCommand {IsActive = true};

            // During execution set the second command to inactive, this should not affect the currently
            // executed selection.  

            firstCommand.ExecuteAction += parameter => secondCommand.IsActive = false;
            var compositeCommand = new CompositeCommand(true);

            compositeCommand.RegisterCommand(firstCommand);
            compositeCommand.RegisterCommand(secondCommand);

            compositeCommand.Execute(null);

            Assert.IsTrue(secondCommand.WasExecuted);
        }

        [Test]
        public void ShouldKeepWeakReferenceToOnCanExecuteChangedHandlers()
        {
            var command = new TestableCompositeCommand();

            var handlers = new CanExecutChangeHandler();
            var weakHandlerRef = new WeakReference(handlers);
            command.CanExecuteChanged += handlers.CanExecuteChangeHandler;
            // ReSharper disable once RedundantAssignment
            handlers = null;

            GC.Collect();

            Assert.IsFalse(weakHandlerRef.IsAlive);
            Assert.IsNotNull(command); // Only here to ensure command survives optimizations and the GC.Collect
        }

        [Test]
        public void ShouldNotMonitorActivityIfUseActiveMonitoringFalse()
        {
            var mockCommand = new MockActiveAwareCommand {IsValid = true, IsActive = true};
            var nonActiveAwareCompositeCommand = new CompositeCommand(false);
            var canExecuteChangedRaised = false;
            nonActiveAwareCompositeCommand.RegisterCommand(mockCommand);
            nonActiveAwareCompositeCommand.CanExecuteChanged += delegate { canExecuteChangedRaised = true; };

            mockCommand.IsActive = false;

            Assert.IsFalse(canExecuteChangedRaised);

            nonActiveAwareCompositeCommand.Execute(null);

            Assert.IsTrue(mockCommand.WasExecuted);
        }

        [Test]
        public void ShouldReraiseCanExecuteChangedEvent()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand {CanExecuteValue = true};

            Assert.IsFalse(multiCommand.CanExecuteChangedRaised);
            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.CanExecuteChangedRaised = false;

            testCommandOne.FireCanExecuteChanged();
            Assert.IsTrue(multiCommand.CanExecuteChangedRaised);
        }

        [Test]
        public void ShouldReraiseCanExecuteChangedEventAfterCollect()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand {CanExecuteValue = true};

            Assert.IsFalse(multiCommand.CanExecuteChangedRaised);
            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.CanExecuteChangedRaised = false;

            GC.Collect();

            testCommandOne.FireCanExecuteChanged();
            Assert.IsTrue(multiCommand.CanExecuteChangedRaised);
        }

        [Test]
        public void ShouldReraiseDelegateCommandCanExecuteChangedEventAfterCollect()
        {
            var multiCommand = new TestableCompositeCommand();
            var delegateCommand = new DelegateCommand<object>(delegate { });

            Assert.IsFalse(multiCommand.CanExecuteChangedRaised);
            multiCommand.RegisterCommand(delegateCommand);
            multiCommand.CanExecuteChangedRaised = false;

            GC.Collect();

            delegateCommand.RaiseCanExecuteChanged();
            Assert.IsTrue(multiCommand.CanExecuteChangedRaised);
        }

        [Test]
        public void UnregisterCommandDisconnectsCanExecuteChangedDelegate()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand {CanExecuteValue = true};

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.UnregisterCommand(testCommandOne);
            multiCommand.CanExecuteChangedRaised = false;
            testCommandOne.FireCanExecuteChanged();
            Assert.IsFalse(multiCommand.CanExecuteChangedRaised);
        }

        [Test]
        public void UnregisterCommandRemovesFromExecuteDelegation()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand {CanExecuteValue = true};

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.UnregisterCommand(testCommandOne);

            Assert.IsFalse(testCommandOne.ExecuteCalled);
            multiCommand.Execute(null);
            Assert.IsFalse(testCommandOne.ExecuteCalled);
        }

        [Test]
        public void UnregisterCommandShouldRaiseCanExecuteEvent()
        {
            var multiCommand = new TestableCompositeCommand();
            var testCommandOne = new TestCommand();

            multiCommand.RegisterCommand(testCommandOne);
            multiCommand.CanExecuteChangedRaised = false;
            multiCommand.UnregisterCommand(testCommandOne);

            Assert.IsTrue(multiCommand.CanExecuteChangedRaised);
        }
    }

    internal class MockActiveAwareCommand : IActiveAware, ICommand
    {
        public Action<object> ExecuteAction;
        private bool _isActive;

        #region IActiveAware Members

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (_isActive != value)
                {
                    _isActive = value;
                    OnActiveChanged(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        public bool WasExecuted { get; set; }
        public bool IsValid { get; set; }

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            return IsValid;
        }

        public event EventHandler CanExecuteChanged = delegate { };

        public void Execute(object parameter)
        {
            WasExecuted = true;
            if (ExecuteAction != null)
                ExecuteAction(parameter);
        }

        #endregion

        protected virtual void OnActiveChanged(object sender, EventArgs e)
        {
            IsActiveChanged(sender, e);
        }
    }

    internal class TestableCompositeCommand : CompositeCommand
    {
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly EventHandler _handler;
        public bool CanExecuteChangedRaised;

        public TestableCompositeCommand()
        {
            _handler = ((sender, e) => CanExecuteChangedRaised = true);
            CanExecuteChanged += _handler;
        }
    }

    internal class TestCommand : ICommand
    {
        public bool CanExecuteValue = true;
        public bool CanExecuteCalled { get; set; }
        public bool ExecuteCalled { get; set; }
        public int ExecuteCallCount { get; set; }

        public void FireCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            CanExecuteCalled = true;
            return CanExecuteValue;
        }

        public event EventHandler CanExecuteChanged = delegate { };

        public void Execute(object parameter)
        {
            ExecuteCalled = true;
            ExecuteCallCount += 1;
        }

        #endregion
    }

    internal class BadDivisionCommand : ICommand
    {
        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            throw new DivideByZeroException("Test Divide By Zero");
        }

        #endregion
    }

    internal class SelfUnregisterableCommand : ICommand
    {
        public CompositeCommand Command;
        public bool ExecutedCalled = false;

        public SelfUnregisterableCommand(CompositeCommand command)
        {
            Command = command;
        }

        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            Command.UnregisterCommand(this);
            ExecutedCalled = true;
        }

        #endregion
    }
}